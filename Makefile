#!make
MAKEFLAGS += --silent

dev:
	@NODE_ENV=development \
	  node_modules/.bin/nodemon --watch "src/**/*.ts" --exec ts-node src/server.ts

prod:
	@NODE_ENV=production \
	  node .

test:
	@NODE_ENV=test \
	  node_modules/.bin/jest

.PHONY: dev prod test
