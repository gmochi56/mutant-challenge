FROM node:12-alpine

# Env
ENV NODE_ENV="production"

WORKDIR /usr/app

COPY package.json .
RUN npm install

ADD . .
RUN npm run build

CMD [ "npm", "start" ]
EXPOSE 8080
