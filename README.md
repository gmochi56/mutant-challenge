# Desafio Mutant
Desafio Desenvolvedor Node.js Mutant
----------

## Sumário
- [Desafio Mutant](#desafio-mutant)
  - [Desafio Desenvolvedor Node.js Mutant](#desafio-desenvolvedor-nodejs-mutant)
  - [Sumário](#sum%c3%a1rio)
  - [Primeiros passos](#primeiros-passos)
    - [Máquina local](#m%c3%a1quina-local)
      - [Instalação das dependências](#instala%c3%a7%c3%a3o-das-depend%c3%aancias)
      - [Execução do projeto](#execu%c3%a7%c3%a3o-do-projeto)
        - [Ambiente de desenvolvimento](#ambiente-de-desenvolvimento)
        - [Ambiente de produção](#ambiente-de-produ%c3%a7%c3%a3o)
    - [Docker](#docker)
      - [Instalação das dependências](#instala%c3%a7%c3%a3o-das-depend%c3%aancias-1)
      - [Execução do projeto](#execu%c3%a7%c3%a3o-do-projeto-1)
    - [Vagrant](#vagrant)
      - [Instalação das dependências](#instala%c3%a7%c3%a3o-das-depend%c3%aancias-2)
      - [Execução do projeto](#execu%c3%a7%c3%a3o-do-projeto-2)
  - [Testando a aplicação](#testando-a-aplica%c3%a7%c3%a3o)
    - [Listar os websites de todos os usuários](#listar-os-websites-de-todos-os-usu%c3%a1rios)
    - [Listar o nome, email e a empresa em que trabalha (em ordem alfabética)](#listar-o-nome-email-e-a-empresa-em-que-trabalha-em-ordem-alfab%c3%a9tica)
    - [Mostrar todos os usuários que no endereço contem a palavra `suite`](#mostrar-todos-os-usu%c3%a1rios-que-no-endere%c3%a7o-contem-a-palavra-suite)
  - [Visualizando os logs](#visualizando-os-logs)
  - [Testes unitários](#testes-unit%c3%a1rios)
  - [Dificuldades](#dificuldades)
  - [Tecnologias utilizadas](#tecnologias-utilizadas)
  - [Versionamento](#versionamento)
  - [Autores](#autores)
  - [Licença](#licen%c3%a7a)

## Primeiros passos
Esse projeto pode ser executado através das seguintes plataformas:
- Máquina local
- Docker
- Vagrant

### Máquina local
#### Instalação das dependências
Execute o arquivo [setup.sh](setup.sh) para que todas as dependências necessárias sejam instaladas na sua máquina:
```
bash setup.sh
```
Esse script instala as dependências elementares para fazer o projeto rodar, que são o Node.js e o Elasticsearch.

#### Execução do projeto
##### Ambiente de desenvolvimento
Para executar o projeto no ambiente de desenvolvimento, executa o comando:
```
npm run dev
```
ou
```
make dev
```

##### Ambiente de produção
```
npm run build
npm start
```
ou
```
make prod
```

### Docker
#### Instalação das dependências
Para executar o projeto via Docker é necessário tê-lo instalado na sua máquina. Caso você não tenha, execute em um terminal os seguintes passos:
```
# Instalando o Docker
sudo apt install docker.io

# Verifique se o Docker foi instalado
docker --version

# Instalando o Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

# Verifique se o Docker Compose foi instalado
docker-compose --version

# Iniciando o serviço do Docker
sudo systemctl start docker
```

#### Execução do projeto
O arquivo [docker-compose.yml](docker-compose.yml) contém as imagens do Node.js e do Elasticsearch. Para instanciar esses serviços, execute o comando:
```
docker-compose up --build
```

### Vagrant
#### Instalação das dependências
Para executar o projeto via Vagrant também é necessário tê-lo instalado na sua máquina. Caso você não tenha, execute em um terminal os seguintes passos:
```
# Instalação o VirtualBox como pré-requisito
sudo apt install virtualbox

# Atualizando os pacotes
sudo apt update

# Baixando o pacto de instalação do repositório
curl -O https://releases.hashicorp.com/vagrant/2.2.6/vagrant_2.2.6_x86_64.deb

# Instalando o pacote baixado
sudo apt install ./vagrant_2.2.6_x86_64.deb

# Verifique se o Vagrant foi instalado
vagrant --version

# Uma vez que o pacote foi instalado com sucesso, remova-o
rm vagrant_2.2.6_x86_64.deb

# Instalando o vagrant-docker-compose plugin para instalar o Docker Compose dentro da VM
vagrant plugin install vagrant-docker-compose
```

#### Execução do projeto
O Vagrant irá executar os projeto dentro da VM através do Docker Compose, por isso foi necessário instalar o plugin `vagrant-docker-compose`. Para iniciar a VM, execute o comando:
```
vagrant up
```

**Em todas as plataformas o servidor Node.js rodará na porta `8080` e o servidor do Elasticsearch na porta padrão `9200`.**

## Testando a aplicação
A aplicação foi construída como uma API HTTP, então foi criado rotas específicas para retornar os dados manipulados dos usuários conforme solicitava o desafio.

### Listar os websites de todos os usuários
```
$ curl -X GET http://localhost:8080/api/user/websites | json_pp

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   136  100   136    0     0   1373      0 --:--:-- --:--:-- --:--:--  1373

[
  "hildegard.org",
  "anastasia.net",
  "ramiro.info",
  "kale.biz",
  "demarco.info",
  "ola.org",
...
```

### Listar o nome, email e a empresa em que trabalha (em ordem alfabética)
```
$ curl -X GET http://localhost:8080/api/user/basic-info | json_pp

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  1860  100  1860    0     0  14418      0 --:--:-- --:--:-- --:--:-- 14418

[
  {
    "name" : "Nicholas Runolfsdottir V",
    "email" : "Sherwood@rosamond.me",
    "company" : {
      "catchPhrase" : "Implemented secondary concept",
      "name" : "Abernathy Group",
      "bs" : "e-enable extensible e-tailers"
    }
  },
...
```

### Mostrar todos os usuários que no endereço contem a palavra `suite`
```
$ curl -X GET http://localhost:8080/api/user/suites | json_pp

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  2843  100  2843    0     0  26570      0 --:--:-- --:--:-- --:--:-- 26570

[
  {
    "address" : {
      "zipcode" : "90566-7771",
      "city" : "Wisokyburgh",
      "street" : "Victor Plains",
      "suite" : "Suite 879",
      "geo" : {
        "lng" : "-34.4618",
        "lat" : "-43.9509"
      }
    },
    "phone" : "010-692-6593 x09125",
    "username" : "Antonette",
    "company" : {
      "bs" : "synergize scalable supply-chains",
      "name" : "Deckow-Crist",
      "catchPhrase" : "Proactive didactic contingency"
    },
    "id" : 2,
    "email" : "Shanna@melissa.tv",
    "website" : "anastasia.net",
    "name" : "Ervin Howell"
  },
...
```

## Visualizando os logs
Como um dos itens do desafio era salvar os logs de todas as interações no Elasticsearch, então, além de pode ver os logs de informação e de erro na pasta `logs` da aplicação, também é possível fazer consultas no Elasticsearch para ver esses mesmos logs.

Para consultar todos os logs no Elasticsearch, basta executar o seguinte commando:
```
curl -X GET http://localhost:9200/_search | json_pp
```

Para consultar os logs no Elasticsearch de um dia específico, basta executar o seguinte comando:
```
curl -X GET http://localhost:9200/logs-<ano>.<mes>.<dia>/_search
# ex.: http://localhost:9200/logs-2020.03.27/_search
```

## Testes unitários
A aplicação conta com a biblioteca Jest para fazer os testes unitários.

Para executar os testes unitários, execute o comando:
```
npm run test
```
ou
```
make test
```

## Dificuldades
Durante o desenvolvimento do projeto, tive problemas em testar a execução da aplicação no Vagrant, pois na hora de criar a imagem Linux na VM, essa ficava aguardando "eternamente" o comando `random: crng init done` ([veja mais](https://unix.stackexchange.com/questions/442698/when-i-log-in-it-hangs-until-crng-init-done)). Portanto, não consegui na minha máquina fazer com a aplicação fosse executada na VM do Vagrant dado esse problema.

## Tecnologias utilizadas
- [Node.js](https://nodejs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Jest](https://jestjs.io/)
- [Elasticsearch](https://www.elastic.co/)
- [Docker](https://www.docker.com/)
- [Vagrant](https://www.vagrantup.com/)

## Versionamento
O versionamento desse projeto foi feito utilizando a ferramente [Git](https://git-scm.com/).

## Autores
- **Gabriel Mochi Ribeiro** - Trabalho inicial - [GabrielMochi](https://github.com/GabrielMochi/)

## Licença
Este projeto está licenciado sob a licença MIT - consulte o arquivo [LICENSE.md](LICENSE.md) para obter detalhes.
