import Geo from './Geo'

class Address {

  public street: string
  public suite: string
  public city: string
  public zipcode: string
  public geo: Geo

  public constructor(
    street: string, suite: string, city: string,
    zipcode: string, geo: Geo
  ) {
    this.street = street
    this.suite = suite
    this.city = city
    this.zipcode = zipcode
    this.geo = geo
  }

}

export default Address
