class Company {

  public name: string
  public catchPhrase: string
  public bs: string

  public constructor(name: string, catchPhrase: string, bs: string) {
    this.name = name
    this.catchPhrase = catchPhrase
    this.bs = bs
  }

}

export default Company
