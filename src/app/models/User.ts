import Address from './Address'
import Company from './Company'

class User {

  public readonly id: number
  public name: string
  public username: string
  public email: string
  public address: Address
  public phone: string
  public website: string
  public company: Company

  public constructor(
    id: number, name: string, username: string,
    email: string, address: Address, phone: string,
    website: string, company: Company
  ) {
    this.id = id
    this.name = name
    this.username = username
    this.email = email
    this.address = address
    this.phone = phone
    this.website = website
    this.company = company
  }

}

export interface UserBasicInfo {
  name: string
  email: string
  company: Company
}

export default User
