class Geo {

  public lat: string // latitude
  public lng: string // longitude

  public constructor(lat: string, lng: string) {
    this.lat = lat
    this.lng = lng
  }

}

export default Geo
