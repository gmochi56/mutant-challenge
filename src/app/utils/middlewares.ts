import boom, { Boom } from '@hapi/boom'
import { NextFunction, Request, RequestHandler, Response } from 'express'

export const asyncMiddleware = (fn: RequestHandler): RequestHandler => (
  (req: Request, res: Response, next: NextFunction): void => {
    Promise.resolve(fn(req, res, next))
      .catch((err: Boom): void => (
        next(err.isBoom ? err : boom.badImplementation(err.message))
      ))
  }
)
