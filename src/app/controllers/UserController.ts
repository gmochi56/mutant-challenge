import { Request, Response } from 'express'

import User, { UserBasicInfo } from '../models/User'
import UserRepository from '../repository/UserRepository'
import Controller from './Controller'


class UserController extends Controller<User, number, UserRepository> {

  public constructor() {
    super(new UserRepository())
  }

  public async getAll(req: Request, res?: Response): Promise<void> {
    const users: User[] = await this.repository.getAll()
    res.json(users)
  }

  public async getWebsites(req: Request, res?: Response): Promise<void> {
    const users: User[] = await this.repository.getAll()
    const websites: string[] = users.map<string>(({ website }: User): string => website)

    res.json(websites)
  }

  public async getUsersBasicInfo(req: Request, res?: Response): Promise<void> {
    const users: User[] = await this.repository.getAll()

    const basicUsersInfo: UserBasicInfo[] = users
      .map<UserBasicInfo>(({ name, email, company }:User): UserBasicInfo => (
        { name, email, company }
      ))
      .sort((a: UserBasicInfo, b: UserBasicInfo): number => {
        const companyNameA: string = a.company.name.toLowerCase()
        const companyNameB: string = b.company.name.toLowerCase()

        return (companyNameA > companyNameB)
          ? 1
          : ((companyNameA < companyNameB)
            ? -1
            : 0)
      })

      res.json(basicUsersInfo)
  }

  public async getUsersWithSuite(req: Request, res?: Response): Promise<void> {
    const users: User[] = await this.repository.getAll()

    const filteredUsers: User[] = users.filter(
      ({ address: { suite } }: User): boolean => suite.toLowerCase().includes('suite')
    )

    res.json(filteredUsers)
  }

  public get(req: Request, res?: Response): void {
    throw new Error('Method not implemented.')
  }

  public create(req: Request, res?: Response): void {
    throw new Error('Method not implemented.')
  }

  public update(req: Request, res?: Response): void {
    throw new Error('Method not implemented.')
  }

  public delete(req: Request, res: Response): void {
    throw new Error('Method not implemented.')
  }

}

export default UserController
