import { NextFunction, Request, Response } from 'express'

import Repository from '../repository/Repository'


abstract class Controller<T, I, R extends Repository<T, I>> {

  protected repository: R

  protected constructor(repository: R) {
    this.repository = repository
  }

  protected abstract getAll(
    req: Request, res?: Response, next?: NextFunction
  ): void | Promise<void>

  protected abstract get(
    req: Request, res?: Response, next?: NextFunction
  ): void | Promise<void>

  protected abstract create(
    req: Request, res?: Response, next?: NextFunction
  ): void | Promise<void>

  protected abstract update(
    req: Request, res?: Response, next?: NextFunction
  ): void | Promise<void>

  protected abstract delete(
    req: Request, res?: Response, next?: NextFunction
  ): void | Promise<void>

}

export default Controller
