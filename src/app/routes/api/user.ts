import { Request, Response, Router } from 'express'

import UserController from '../../controllers/UserController'
import { asyncMiddleware } from '../../utils/middlewares'


const router: Router = Router()
const userController: UserController = new UserController()

router.get('/', asyncMiddleware(async (req: Request, res: Response): Promise<void> => {
  await userController.getAll(req, res)
}))

router.get('/websites', asyncMiddleware(async (req: Request, res: Response): Promise<void> => {
  await userController.getWebsites(req, res)
}))

router.get('/basic-info',  asyncMiddleware(async (req: Request, res: Response): Promise<void> => {
  await userController.getUsersBasicInfo(req, res)
}))

router.get('/suites',  asyncMiddleware(async (req: Request, res: Response): Promise<void> => {
  await userController.getUsersWithSuite(req, res)
}))

export default router
