import bodyParser from 'body-parser'
import cors from 'cors'
import { Router } from 'express'
import morgan from 'morgan'

import logger from '../../config/logger'
import api from './api'


const router: Router = Router()

router.use(cors({ origin: true }))

router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: true }))

router.use(morgan('combined', {
  stream: {
    write: (message: string): void => {
      logger.info(message)
    }
  }
}))

router.use('/api', api)

export default router
