import boom, { Boom } from '@hapi/boom'
import express, { Express, NextFunction, Request, Response } from 'express'

import logger from '../config/logger'
import routes from './routes'

const app: Express = express()

app.use('/', routes)

app.use((err: Boom, req: Request, res: Response, next: NextFunction): void => {
  err = err.isBoom ? err : boom.badImplementation(err.message)

  if (err.isServer) logger.error(err)

  res.status(err.output.statusCode).json(err.output.payload)
})

export default app
