import { AxiosResponse } from 'axios'

import User from '../models/User'
import Repository from './Repository'

class UserRepository  extends Repository<User, number> {

  public constructor() {
    super(`${process.env.BASE_API_URL}/users`)
  }

  public async getAll(): Promise<User[]> {
    const { data: users }: AxiosResponse<User[]> = await this.fetcher.get<User[]>('/')
    return users
  }

  public get(id: number): Promise<User> {
    throw new Error('Method not implemented.')
  }

  public create(element: User): Promise<User> {
    throw new Error('Method not implemented.')
  }

  public update(element: User): Promise<User> {
    throw new Error('Method not implemented.')
  }

  public delete(id: number): Promise<void> {
    throw new Error('Method not implemented.')
  }

}

export default UserRepository
