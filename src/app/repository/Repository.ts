import axios, { AxiosInstance } from 'axios'

/**
 * @param endpoint Data URL endpoint
 *
 * @template T Object Type
 * @template I Object Id Type
 */
abstract class Repository<T, I> {

  protected fetcher: AxiosInstance
  protected endpoint: string

  protected constructor(endpoint: string) {
    this.endpoint = endpoint

    this.fetcher = axios.create({
      baseURL: endpoint
    })
  }

  protected abstract getAll(): [T] | Promise<T[]>
  protected abstract get(id: I): T | Promise<T>
  protected abstract create(element: T): T | Promise<T>
  protected abstract update(element: T): T | Promise<T>
  protected abstract delete(id: I): void | Promise<void>

}

export default Repository
