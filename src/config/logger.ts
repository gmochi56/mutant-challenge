import path from 'path'
import { createLogger, format, Logger, transports } from 'winston'
import { Client } from '@elastic/elasticsearch'
import ElasticSearchTransport from 'winston-elasticsearch'

const infoLogFilePath: string = path.resolve(__dirname, '../../logs/info.log')
const errorLogFilePath: string = path.resolve(__dirname, '../../logs/error.log')

const client: Client = new Client({ node: process.env.ELASTICSEARCH_NODE })

const logger: Logger = createLogger({
  transports: [
    new ElasticSearchTransport({
      level: 'info',
      client
    }),
    new transports.Console({
      level: 'debug',
      format: format.simple(),
      handleExceptions: true
    }),
    new transports.File({
      level: 'info',
      filename: infoLogFilePath,
      format: format.simple()
    }),
    new transports.File({
      level: 'error',
      filename: errorLogFilePath,
      format: format.simple(),
      handleExceptions: true
    })
  ]
})

export default logger
