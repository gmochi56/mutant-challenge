import dotenv from 'dotenv'
import fs from 'fs'
import path from 'path'

const defaultEnvironmentPath: string = path.resolve(__dirname, '../../.env')
const customEnvironmentPath: string = path.resolve(__dirname, `../../.env.${process.env.NODE_ENV}`)


dotenv.config({ path: defaultEnvironmentPath })

if (fs.existsSync(customEnvironmentPath))
  dotenv.config({ path: customEnvironmentPath })
