import './config/envs'

import { createServer, Server } from 'http'

import app from './app'
import logger from './config/logger'

const server: Server = createServer(app)
const hostname: string = process.env.HOSTNAME || '0.0.0.0'
const port: number = parseInt(process.env.PORT, 10) || 8080

server.listen(port, hostname, (): void => {
  logger.info(`The server is running on http://${hostname}:${port}`)
})
