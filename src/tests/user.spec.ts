import request from 'supertest'

import '../config/envs'
import app from '../app'

jest.useFakeTimers()

describe('GET /api/user', (): void => {
  it(
    'responds with a list of users',
    (done: jest.DoneCallback): void => {
      request(app)
        .get('/api/user')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done)
    }
  )
})

describe('GET /api/user/websites', (): void => {
  it(
    `responds with a list of users' websites`,
    (done: jest.DoneCallback): void => {
      request(app)
        .get('/api/user/websites')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done)
    }
  )
})

describe('GET /api/user/basic-info', (): void => {
  it(
    `responds with a list of users' basic information`,
    (done: jest.DoneCallback): void => {
      request(app)
        .get('/api/user/basic-info')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done)
    }
  )
})

describe('GET /api/user/suites', (): void => {
  it(
    'responds with a list of users that are hosted in suites',
    (done: jest.DoneCallback): void => {
      request(app)
        .get('/api/user/suites')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done)
    }
  )
})
