#!/bin/bash

sudo apt-get update -y
sudo apt-get upgrade -y

# Install make
sudo apt install make -y

# Install curl
sudo apt install curl

# Install Node.js
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install nodejs -y

sudo apt-get install build-essential -y

# Install Java for Elasticsearch
sudo apt install default-jre -y

# install Elasticsearch
sudo apt-get install apt-transport-https

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo add-apt-repository "deb https://artifacts.elastic.co/packages/7.x/apt stable main"

sudo apt-get update
sudo apt-get install elasticsearch

# Launch Elasticsearch
sudo /bin/systemctl enable elasticsearch.service
sudo service elasticsearch start

# Installing npm dependencies
npm install
